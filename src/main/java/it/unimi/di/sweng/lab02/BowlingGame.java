package it.unimi.di.sweng.lab02;

public class BowlingGame implements Bowling {
	private static final int MAX_ROLLS = 20;
	private static final int MAX_FRAMES = 10;
	
	private Frame[] frames = new Frame[MAX_FRAMES];
	
	private int[] rolls = new int[MAX_ROLLS];
	private int currentFrameIndex = 0;
	
	public BowlingGame() {
		frames[currentFrameIndex] = new Frame();
	}
	
	@Override
	public void roll(int pins) {
		if(!frames[currentFrameIndex].canRoll()){
			frames[++currentFrameIndex] = new Frame();
		}
		frames[currentFrameIndex].roll(pins);
	}

	@Override
	public int score() {
		int score = 0;
		for(int i=0; i<=currentFrameIndex; i++){
			score += frames[i].score();
			if(i < MAX_FRAMES - 1){
				if(frames[i].isSpare()){
					score += frames[i + 1].rolls[0];
				}
				if(frames[i].isStrike()){
					score += frames[i + 1].score();
				}
			}
		}
		return score;
	}
}
