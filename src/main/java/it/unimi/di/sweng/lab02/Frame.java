package it.unimi.di.sweng.lab02;

public class Frame {
	private static final int MAX_ROLLS = 2;
	private static final int MAX_SCORE = 10;

	public int[] rolls = new int[MAX_ROLLS];
	private int currentRoll = 0;
	
	public boolean canRoll(){
		if(currentRoll < MAX_ROLLS){
			if(isStrike())
				return false;
			return (score() <= MAX_SCORE);
		}
		return false;
	}
	
	public Frame(){
		for(int i=0; i<MAX_ROLLS; i++)
			rolls[i] = 0;
	}
	
	public void roll(int pins) {
		rolls[currentRoll++] = pins;
	}
	
	public int score() {
		if(isStrike()){
			return rolls[0];
		}
		return rolls[0] + rolls[1];
	}

	public boolean isStrike(){
		return rolls[0] == 10;
	}
	
	public boolean isSpare(){
		return !isStrike() && rolls[0] + rolls[1] == 10;
	}
}
